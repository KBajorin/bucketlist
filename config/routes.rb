Bucketlist::Application.routes.draw do
  resources :comments, :only => [:create]

  resources :destinations, :only => [:create, :destroy, :index]

 resources :todo_items do
 	member do
 		post :like
 	end

   collection do
  	post :update_position
  end
end
   root :to => 'todo_items#index'

end
