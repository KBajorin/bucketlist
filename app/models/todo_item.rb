class TodoItem < ActiveRecord::Base

  attr_accessible :completed, :name, :location, :latitude, :longitude, :destination_id

  validates :location, :presence => :true

  geocoded_by :geocoding_method
  after_validation :geocode

  has_many :comments, :dependent => :destroy
  belongs_to :destination
  belongs_to :user
  has_many :likes
  has_many :users, :through => :likes

  def geocoding_method
  	"#{self.location} #{destination.name}"

  end


end
