class TodoItemsController < ApplicationController

	def index
		# @items = TodoItem.all
		order = params[:order] || :created_at
		@destinations = Destination.all
		@items = TodoItem.order(order).all
		respond_to do |format|
			format.html
			format.js{
				render :template => 'todo_items/order', :locals => {:todo_items => @items}
			}
		end
	end

	def like
		todo_item = TodoItem.find(params[:id])

		Like.create(:todo_item_id => todo_item.id, :user_id => User.first.id)
		render :nothing => true

	end


	def create

		@item = TodoItem.new(params[:todo_item])
		@item.save!
		render @item
	end

	def destroy

   	 	@item = TodoItem.find(params[:id])
    	@item.destroy
     	respond_to do |format|
     	 format.html { redirect_to todo_items_url }

    	end
    end


      def update_position
    params[:results].each do |i,results|
      TodoItem.update_all("position = #{results[1]}", "id = #{results[0]}")

  	end
      render :text => "Nice one"
    end



end
