class DestinationsController < ApplicationController


	def index

		@destination = Destination.find(params[:id])

lat = params[:latitude] || 0
    long = params[:longitude] || 0

    @destination.near(params[:latitude], params[:longitude])
	end

	def create
		@destination = Destination.new(params[:destination])
		@destination.save!
		render @destination
	end

		def destroy

   	 	@destination = Destination.find(params[:id])
   	 	@destination.todo_items.each do |d|
   	 		d.destroy
   	 		d.comments.each do |c|
   	 			c.destroy
   	 		end
   	 	end

    	@destination.destroy
     	respond_to do |format|
     	 format.html { redirect_to todo_items_url }

    	end
    end
end
