
function createTable(data) {

var data = [
    { label: 'one', value: 25 },
    { label: 'two', value: 25 },
    { label: 'three', value: 25 },
    { label: 'four', value: 5 },
    { label: 'five', value: 20 }
  ]
new Morris.Donut({
  // ID of the element in which to draw the chart.
  element: 'chart',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data: data,
  // The name of the data record attribute that contains x-values.
  xkey: 'label',
  // A list of names of data record attributes that contain y-values.
  ykeys: 'value',
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Value'],

  colors: ['#a200ff','#00aedb','#48a627','#971317','#18e99f']

});

}




